different tools for winman

## install\_python\_3.bat
This is a wrapper script for install\_python\_3.ps1 to always assure administrative privileges and to start the powershell script in the right directory

# install\_python\_3.ps1
_Only call this script with the install\_python\_3.bat wrapper._

This script searches for the current python version, downloads and installs it and additionally installs pypiwin32 with pip as this is needed for winman

# zip\_extract.py
extract contents of a zip archive to a specified destination without common prefixes shared across all files in the archive, e.g. if all files start with '/folder1/folder2/' skip these two folders
