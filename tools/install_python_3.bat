@echo off
call :check_permissions
echo.

start /d %~dp0 /b /wait powershell.exe -noLogo -ExecutionPolicy unrestricted -file %~dp0\install_python_3.ps1

exit /b 0

:check_permissions
    echo administrative permissions required, detecting permissions ...
    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo administrative permissions granted
    ) else (
        echo error: missing administrative permissions
        echo press any key to exit
        pause >nul
        exit
    )
exit /b 0