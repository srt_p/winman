# use TLS 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# from https://stackoverflow.com/questions/21422364/is-there-any-way-to-monitor-the-progress-of-a-download-using-a-webclient-object
function download_file($url, $targetFile) {
    $uri = New-Object "System.Uri" "$url"
    $request = [System.Net.HttpWebRequest]::Create($uri)
    $request.set_Timeout(15000) # 15 second timeout
    $response = $request.GetResponse()
    $totalLength = [System.Math]::Floor($response.get_ContentLength()/1024)
    $responseStream = $response.GetResponseStream()
    $targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $targetFile, Create
    $buffer = new-object byte[] 10KB
    $count = $responseStream.Read($buffer,0,$buffer.length)
    $downloadedBytes = $count
    while($count -gt 0) {
        $targetStream.Write($buffer, 0, $count)
        $count = $responseStream.Read($buffer,0,$buffer.length)
        $downloadedBytes = $downloadedBytes + $count
        Write-Progress -activity "downloading file '$($url.split('/') | Select -Last 1)'" -status "downloaded ($([System.Math]::Floor($downloadedBytes/1024))K of $($totalLength)K): " -PercentComplete ((([System.Math]::Floor($downloadedBytes/1024)) / $totalLength)  * 100)
    }
    Write-Progress -activity "finished downloading file '$($url.split('/') | Select -Last 1)'"
    $targetStream.Flush()
    $targetStream.Close()
    $targetStream.Dispose()
    $responseStream.Dispose()
    Write-Progress -Activity "downloading file '$($url.split('/') | Select -Last 1)'" -Status "Ready" -Completed
}

function remote_file_size($url) {
    $uri = New-Object "System.Uri" "$url"
    $request = [System.Net.HttpWebRequest]::Create($uri)
    $request.set_Timeout(15000)
    $response = $request.getResponse()
    return $response.get_ContentLength()
}

# write 4 newlines (2 are already written from batch wrapper, 4 are for Write-Progress
Write-Output ""
Write-Output ""
Write-Output ""
Write-Output ""

# extract current python version
$httpcontent = Invoke-WebRequest -URI "https://www.python.org/downloads/windows/" -UseBasicParsing
$found = $httpcontent -match 'Latest Python 3 Release - Python ([0-9]+(\.[0-9])*)'
if($found) {
    $version = $matches[1]
} else {
    Write-Output "error: cannot extract current python 3 version"
    Write-Output "press any key to exit ..."
    cmd /c Pause | Out-Null
    break
}
Write-Output "current python version: $version"

# get os architecture
$cpu_info = Get-WmiObject -class Win32_Processor
foreach($cpu_values in $cpu_info) {
    $arch = $cpu_values.Architecture
    break
}
if($arch -eq 0) {
    $bits = 32
} elseif($arch -eq 9) {
    $bits = 64
} else {
    Write-Output "error: unrecognized system architecture, cannot detect 32 or 64 bit"
    Write-Output "press any key to exit ..."
    cmd /c Pause | Out-Null
    break
}
Write-Output "system architecture: $bits-bit"

# download corresponding file if necessary
$fileurl = "https://www.python.org/ftp/python/$version/python-$version" + (&{if($bits -eq 64) {"-amd64"}}) + ".exe"
$remotefilesize = remote_file_size("$fileurl")
$filename = "cache\$($fileurl.split('/') | Select -Last 1)"
$filesize = (Get-Item "$filename").length
if(-Not(Test-Path "cache")) {
    New-Item -ItemType Directory -Force -Path "cache" | Out-Null
}
if(-Not((Test-Path "$filename") -And ($remotefilesize -eq (Get-Item "$filename").length))) {
    download_file "$fileurl" "$filename"
}

# install
$installation_directory = [System.Environment]::ExpandEnvironmentVariables("%ProgramFiles%\python3")
Write-Output "installation directory: $installation_directory"
Start-Process -FilePath "./$filename" -ArgumentList "/quiet InstallAllUsers=1 PrependPath=1 Shortcuts=0 TargetDir=`"$installation_directory`"" -Wait -NoNewWindow
$env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine")

# install pypiwin32
Write-Output "installing pywin32..."
Start-Process -FilePath "python" -ArgumentList "-m pip install -U pywin32" -Wait -NoNewWindow
$python_dir = Split-Path (Get-Command python | Select-Object -ExpandProperty Definition)
Start-Process -FilePath "python" -ArgumentList "`"$python_dir\Scripts\pywin32_postinstall.py`" -install" -Wait -NoNewWindow

# done
Write-Output "done"
Write-Output "press any key to exit..."
# pause
cmd /c Pause | Out-Null
