#! python3

# extract zipfile without a top-level-directory if present
# usage: ... <sourcefile> <destination>

from zipfile import *
import sys

def common_prefix(strings):
    """https://stackoverflow.com/questions/6718196/determine-prefix-from-a-set-of-similar-strings"""
    try:
        str1 = min(strings)
        str2 = max(strings)
        for i, c in enumerate(str1):
            if c != str2[i]:
                return str2[:i]
        return str1
    except:
        return ''

def delete_top_dir(zf):
    prefix = common_prefix([zi.filename for zi in zf.infolist()])
    prefix_len = len(prefix)
    for zi in zf.infolist():
        if zi.filename == prefix:
            # if only one file in zip
            return zi
        zi.filename = zi.filename[prefix_len:]
        yield zi

zf = ZipFile(sys.argv[1])
zf.extractall(sys.argv[2], delete_top_dir(zf))