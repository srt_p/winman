#! python3

# references:
# for admin execution of subprocesses: https://stackoverflow.com/questions/130763/request-uac-elevation-from-within-a-python-script
# for admin execution of subprocesses: https://pastebin.com/qeeF58EC
# for terminal size: https://stackoverflow.com/questions/566746/how-to-get-linux-console-window-width-in-python
# for architecture reconition: https://stackoverflow.com/questions/9964396/python-check-if-a-system-is-32-or-64-bit-to-determine-whether-to-run-the-funct
# for file download: https://stackoverflow.com/questions/22676/how-do-i-download-a-file-over-http-using-python
# for environment update: https://stackoverflow.com/questions/21138014/how-to-add-to-and-remove-from-systems-environment-variable-path

from winman_util import *
from winman_error import *
from winman_operation_version import *
from winman_operation_sync import *

class Parser():
    def parse_ops(self, argv, ops):
        argv = argv[1:]

        # find used operation
        self.op = None
        for i in range(len(argv)):
            for op in ops:
                if op.regex.match(argv[i]):
                    if self.op == None or self.op == op:
                        # remove op from argv
                        if argv[i] == '-' + op.shortstr or argv[i] == '--' + op.longstr:
                            argv[i] = ''
                        else:
                            argv[i] = argv[i].replace(op.shortstr, '', 1)
                        self.op = op
                    else:
                        raise WinmanError('only one operation may be used at a time')

        # one exception: the help operation
        if not self.op:
            print_help = False
            for arg in argv:
                if re.compile('((\s|\A)-[a-zA-Z]*h[a-zA-Z]*(\s|$))|((\s|\A)--help(\s|$))').match(arg):
                    self.print_help_ops(ops)
                    print_help = True
                    break
            if not print_help:
                raise WinmanError('no operation specified (use -h for help)')

        # remove empty strings from argv
        return [x for x in argv if x]

    def parse_opts(self, argv, opts):
        self.opts = []
        next_is_arg_val = False
        processing_next_argv = False # True if one complete argv was processed last time
        done = False
        while argv and not done:
            if next_is_arg_val and processing_next_argv:
                self.opts[-1].argument_value = argv[0]
                del argv[0]
                next_is_arg_val = False
                continue
            processing_next_arg = False
            for i in range(len(opts)):
                opt = opts[i]
                if opt.regex.match(argv[0]):
                    if opt.unique:
                        if sum(1 for x in self.opts if x.longstr == opt.longstr):
                            raise WinmanError('option is unique -- ' + opt.longstr)
                    if opt.argument:
                        if next_is_arg_val:
                            raise WinmanError('cannot use two shortstrings with expected arguments in the same argv')
                        next_is_arg_val = True
                    # remove opt from argv
                    if argv[0] == '-' + opt.shortstr or argv[0] == '--' + opt.longstr:
                        del argv[0]
                        processing_next_argv = True
                    else:
                        argv[0] = argv[0].replace(opt.shortstr, '', 1)
                    self.opts.append(opt.clone())
                    break
                # no opt fits -> targets are now starting
                if i == len(opts) - 1:
                    done = True

        if next_is_arg_val:
            raise WinmanError('option requires an argument -- ' + self.opts[-1].longstr)

        return [x for x in argv if x]

    def print_help_ops(self, ops):
        print('usage: winman <operation> [...]')
        print('operations:')
        help_string = '    winman {-h --help}'
        print_help_entry(help_string, len(help_string), '')
        first_strings = []
        for op in ops:
            first_strings.append('    winman {-' + op.shortstr + ' --' + op.longstr + '}')
        max_len_first_string = max(map(len, first_strings))
        for i in range(len(ops)):
            print_help_entry(first_strings[i], max_len_first_string,
                             ('[options]' if ops[i].opts else '') + (' ' if ops[i].opts and ops[i].targets_name else '') +
                             (('[' + ops[i].targets_name + ']') if ops[i].targets_name else ''))
        print('\nuse \'winman {-h --help}\' with an operation for available options')

def main(argv):
    # fill valid operations
    ops = [OpVersion(), OpSync()]
    try:
        pars = Parser()
        argv = pars.parse_ops(argv, ops)
        if pars.op:
            argv = pars.parse_opts(argv, pars.op.opts)
            with open('winman_repository.json', 'r') as file:
                repo = json.load(file)
            with open('winman_updater.json', 'r') as file:
                updater = json.load(file)
            pars.op.execute(pars.opts, argv, repo, updater)
    except WinmanError as e:
        print(('\n' if WinmanError.newline_needed_before else '') + 'error: ' + str(e))
    except KeyboardInterrupt as e:
        print(('\n' if WinmanError.newline_needed_before else '') + 'Interrupt signal received')

if __name__ == "__main__":
    main(sys.argv)
