import re
from winman_util import *

class Op:
    class Opt:
        def __init__(self, longstr, shortstr = '', desc = '', unique = False, argument = False, argument_value = '', argument_name = ''):
            self.longstr = longstr
            self.shortstr = shortstr
            self.desc = desc
            self.unique = unique
            self.argument = argument
            self.argument_value = argument_value
            self.argument_name = argument_name
            self.regex = re.compile(('' if not self.shortstr else '((\s|\A)-[a-zA-Z]*' + self.shortstr + '[a-zA-Z]*(\s|$))|') + '((\s|\A)--' + self.longstr + '(\s|$))')

        def clone(self):
            ret = Op.Opt(self.longstr, self.shortstr, self.desc, self.unique, self.argument, self.argument_value, self.argument_name)
            ret.regex = self.regex
            return ret

    def __init__(self, longstr, shortstr, opts, targets_name = 'package(s)'):
        self.longstr = longstr
        self.shortstr = shortstr
        self.regex = re.compile('((\s|\A)-[a-zA-Z]*' + self.shortstr + '[a-zA-Z]*(\s|$))|((\s|\A)--' + self.longstr + '(\s|$))')
        self.opts = opts
        self.targets_name = targets_name

    def opts_to_dict(self, opts):
        """converts list of options to dict of form 'name: x' where x is either the number of option occurences or a list of arguments passed.
        All possible options are included, so x can be 0 or []"""
        odict = {}
        for opt in self.opts:
            odict[opt.longstr] = [] if opt.argument else 0
        for opt in opts:
            odict[opt.longstr] += [opt.argument_value] if opt.argument else 1
        return odict

    def print_help_opts(self):
        print('usage: winman {-' + self.shortstr + ' --' + self.longstr + '}' + (' [options]' if self.opts else '') + ((' [' + self.targets_name + ']') if self.targets_name else ''))
        print('options:')
        first_strings = []
        for opt in self.opts:
            first_strings.append('  ' + (('-' + opt.shortstr + ',') if opt.shortstr else '   ') + ' --' + opt.longstr + ((' <' + opt.argument_name + '>') if opt.argument else ''))
        max_len_first_string = max(map(len, first_strings))
        for i in range(len(self.opts)):
            print_help_entry(first_strings[i], max_len_first_string, self.opts[i].desc)
            #print(first_strings[i] + (max_len_first_string - len(first_strings[i]) + 1) * ' ' + self.opts[i].desc)
