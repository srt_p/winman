from winman_operation import *
from winman_util import *
import sys
import json

class OpSync(Op):
    def __init__(self):
        opts = [
            self.Opt('help', 'h', 'prints this help'),
            self.Opt('info', 'i', 'view package information'),
            self.Opt('search', 's', 'search repository for matching strings', False, True, '', 'regex'),
            self.Opt('refresh', 'y', 'retrieve fresh package information (-yy forces a refresh of all values even if version is up to date), if targets are specified, only update those')
        ]
        super(OpSync, self).__init__('sync', 'S' , opts)

    def execute(self, opts, targets, repo, updater):
        odict = self.opts_to_dict(opts)
        already_did_something = False
        # check invalid options
        if odict['search'] and odict['info']:
            raise WinmanError('invalid option: \'--info\' and \'--search\' may not be used together')
        if odict['help']:
            self.print_help_opts()
            return
        if odict['search']:
            regs = []
            for regex in odict['search']:
                regs.append(re.compile(regex))
            found_packages = []
            for name, info in repo['packages'].items():
                for reg in regs:
                    if reg.match(name) or reg.match(info['desc']):
                        found_packages.append(name)
                        break
            for name in found_packages:
                print(name + ' ' + repo['packages'][name]['version'])
                print('\n'.join(map(lambda x: '    ' + x, textwrap.wrap(repo['packages'][name]['desc'], width = get_terminal_size_windows((70,10))[0] - 5))))
            return
        if odict['info']:
            first = True
            pdict = {
                'Name': (lambda x: x),
                'Description': (lambda x: repo['packages'][x]['desc']),
                'Installer kind': (lambda x: repo['packages'][x]['kind']),
                'Installer x86': (lambda x: repo['packages'][x]['x86']),
                'Installer x86_64': (lambda x: repo['packages'][x]['x86_64']),
                'Version': (lambda x: repo['packages'][x]['version']),
            }
            for name in targets:
                if not first:
                    print('')
                first = False
                prints = []
                for k, v in pdict.items():
                    try:
                        prints.append((k, v(name)))
                    except:
                        continue
                max_len_first_string = max(map(lambda x: len(x[0]), prints))
                for (x1, x2) in prints:
                    wrapped_text = textwrap.wrap(x2, width = get_terminal_size_windows((70,10))[0] - max_len_first_string - 4)
                    print(x1 + (max_len_first_string - len(x1) + 1) * ' ' + ': ' + wrapped_text[0])
                    for t in wrapped_text[1:]:
                        print((max_len_first_string + 3) * ' ' + t)
            return
        if odict['refresh']:
            already_did_something = True
            target_list = targets if targets else repo['packages']
            printer = NumberedPrinter(len(target_list))
            for name in target_list:
                printer.print('retrieving information for ' + name)
                package_subs = get_package_regex_subs(name, updater)
                package_checksums = get_package_regex_checksums(name, updater, package_subs)
                # don't update the informations if not necessary
                if odict['refresh'] == 1 and 'version' in repo['packages'][name] and repo['packages'][name]['version'] == package_subs['version']:
                    continue
                repo['packages'][name]['version'] = package_subs['version']
                del package_subs['version']
                if package_subs:
                    repo['packages'][name]['subs'] = {}
                    for sub_k, sub_v in package_subs.items():
                        repo['packages'][name]['subs'][sub_k] = sub_v
                if 'x86' in repo['packages'][name]:
                    expanded_file_url = expand_package_string(name, repo, repo['packages'][name]['x86'])
                    file_size = get_remote_file_size(name, expanded_file_url)
                    if file_size:
                        repo['packages'][name]['x86_size'] = file_size
                    if 'x86_checksums' in package_checksums:
                        repo['packages'][name]['x86_checksums'] = package_checksums['x86_checksums']
                if 'x86_64' in repo['packages'][name]:
                    expanded_file_url = expand_package_string(name, repo, repo['packages'][name]['x86_64'])
                    file_size = get_remote_file_size(name, expanded_file_url)
                    if file_size:
                        repo['packages'][name]['x86_64_size'] = file_size
                    if 'x86_64_checksums' in package_checksums:
                        repo['packages'][name]['x86_64_checksums'] = package_checksums['x86_checksums']
            with open('winman_repository.json', 'w') as file:
                json.dump(repo, file, indent = 4)
            return
        if not targets and not already_did_something:
            raise WinmanError('no targets specified (use -h for help)')
        if not targets:
            return

        print('resolving dependencies...')
        targets_to_process = targets
        targets = set()
        innerdeps = {}
        while targets_to_process:
            tar = targets_to_process[0]
            del targets_to_process[0]
            if tar in targets:
                continue
            deps = get_package_deps(tar, repo)
            if deps[0]:
                innerdeps[tar] = deps[0]
            targets_to_process += list(deps[1])
            targets.add(tar)
        targets_inner = set()
        for _, v in innerdeps.items():
            for idep in v:
                if not idep in targets:
                    targets_inner.add(idep)

        # first use all targets including inner ones, delete inners after gathering download information
        install_info = expand_package_info(targets | targets_inner, repo)
        download_size = 0
        max_download_filename_length = 0
        download_info = {}
        for name in install_info:
            if os.path.isfile(install_info[name]['package_filename']):
                if 'package_file_size' in install_info[name]:
                    if os.path.getsize(install_info[name]['package_filename']) == int(install_info[name]['package_file_size']):
                        continue
            download_info[name] = {}
            if not 'package_file_size' in install_info[name]:
                winman_warning('no download size registered for ' + name)
            else:
                download_info[name]['size'] = int(install_info[name]['package_file_size'])
                download_size += download_info[name]['size']
            download_info[name]['url'] = install_info[name]['package_file_url']
            if len(install_info[name]['package_filename']) - len('cache\\') > max_download_filename_length:
                max_download_filename_length = len(install_info[name]['package_filename']) - len('cache\\')
        # save version and package_filename of innerdeps
        # innerdeps then looks like: {
        #     'package_name_1': [
        #         ('innerdep_1_1', 'innerdep_1_1_version', 'innerdep_1_1_filename'),
        #         ('innerdep_1_2', 'innerdep_1_2_version', 'innerdep_1_2_filename'), ...
        #     ], ...
        # }
        innerdeps = {k: [(x, install_info[x]['$version'], install_info[x]['package_filename']) for x in v] for k, v in innerdeps.items()}
        # remove innerdeps from install_info
        print('\nPackages (' + str(len(targets)) + ') ' + '  '.join(sorted([k + '-' + v['$version'] + (('{' + ','.join(sorted(map(lambda x: x[0] + '-' + x[1], innerdeps[k]))) + '}') if k in innerdeps else '') for k, v in install_info.items() if k in targets])) + '\n')
        if download_size:
            print('Total Download Size: ' + human_readable_size(download_size) + '\n')
        if not ask_yes_no(':: Proceed with installation'):
            return
        if download_info:
            print(':: Retrieving packages...')
            printer = NumberedPrinter(len(download_info))
            for name in download_info:
                download_file(name, repo['packages'][name]['version'], download_info[name]['url'], install_info[name]['package_filename'],
                    lambda str1, str2, progress, done: printer.print(
                        auto_suffix(str1, max_download_filename_length) + '  ' + str2 +
                        ' [' + progress_bar(progress, '#', '-', get_terminal_size_windows((70,10))[0] - max_download_filename_length - 12 - printer.prefix_length() - len(str2)) + '] ' +
                        auto_prefix(('%d' % (progress * 100)) + '%', 4),
                        next = done, end = '\n' if done else '\r'))
        install_info = {k: v for k, v in install_info.items() if not k in targets_inner}

        print(':: Processing package changes...')
        printer = NumberedPrinter(len(install_info))
        idict = {
            'nsis': ((lambda package_filename: package_filename), (lambda package_filename, install_dir: '/S /D=' + install_dir)),
            'msi': ((lambda package_filename: 'msiexec.exe'), (lambda package_filename, install_dir: '/i "' + os.path.join(os.path.dirname(os.path.realpath(__file__)), package_filename) + '" /qn')),
            'zip': ((lambda package_filename: 'python.exe'), (lambda package_filename, install_dir: 'tools/zip_extract.py "' + package_filename + '" "' + install_dir + '"')),
            'innosetup': ((lambda package_filename: package_filename), (lambda package_filename, install_dir: '/SP- /VERYSILENT /NORESTART /NOICONS /DIR="' + install_dir + '"'))
        }
        for name in install_info:
            printer.print('installing ' + name)
            #print('filename: ' + install_info[name]['package_filename'])
            #print('install dir: ' + install_info[name]['$instdir'])
            if name in innerdeps:
                indent = 2
                print(' ' * indent + 'installing inner dependencies')
                innerprinter = NumberedPrinter(len(innerdeps[name]), indent)
                for idep, _, idep_filename in innerdeps[name]:
                    innerprinter.print('installing ' + idep)
                    command = idict[repo['packages'][name]['kind']][0](idep_filename)
                    args = idict[repo['packages'][name]['kind']][1](idep_filename, install_info[name]['$instdir'])
                    elevated_execute(command, args)
            command = idict[repo['packages'][name]['kind']][0](install_info[name]['package_filename'])
            args = idict[repo['packages'][name]['kind']][1](install_info[name]['package_filename'], install_info[name]['$instdir'])
            #print('command: ' + command)
            #print('args: ' + args)
            elevated_execute(command, args)
        print(':: Checking and Modifying Path entries...')
        paths_to_add = []
        for name in install_info:
            paths_to_add += install_info[name]['package_paths']
        add_to_path(paths_to_add)
