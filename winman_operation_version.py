from winman_operation import *

version = 'v0.02'

class OpVersion(Op):
    def __init__(self):
        super(OpVersion, self).__init__('version', 'V', [], '')

    def execute(self, opts, targets, repo, updater):
        print('Winman ' + version)
