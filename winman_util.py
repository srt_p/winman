from winman_error import *
import sys
import re
import urllib.request
import time
import subprocess
import os
import ctypes
import ssl
import textwrap
import win32com.shell.shell as win32shell
import win32com.shell.shellcon as win32shellcon
import win32event
import win32gui
import win32con

class NumberedPrinter():
    def __init__(self, max, indent = 0):
        self.max = max
        self.cur = 1
        self.str_max = str(self.max)
        self.str_len_max = len(self.str_max)
        self.indent = indent

    def print(self, string, end = '\n', next = True):
        if '\r' in end:
            WinmanError.newline_needed_before = True
        else:
            WinmanError.newline_needed_before = False
        print(' ' * self.indent + '(' + ' ' * (self.str_len_max - len(str(self.cur))) + str(self.cur) + '/' + self.str_max + ') ' + string, end = end)
        if next:
            self.cur += 1

    def prefix_length(self):
        return 3 + 2 * self.str_len_max

def progress_bar(progress, fill_done, fill_todo, length):
    """return string with 'length' segments consisting of 'fill_done' and 'fill_todo' with the ration of 'progress'"""
    if progress >= 1:
        return fill_done * length
    ret = fill_done * int(progress * length)
    return ret + fill_todo * (length - len(ret))

def auto_suffix(string, wanted_length):
    if wanted_length > len(string):
        return string + ' ' * (wanted_length - len(string))
    else:
        return string

def auto_prefix(string, wanted_length):
    if wanted_length > len(string):
        return ' ' * (wanted_length - len(string)) + string
    else:
        return string

def get_remote_file_size(name, urls):
    try:
        if isinstance(urls, str):
            urls = [urls]
        ssl_back = ssl._create_default_https_context
        ssl._create_default_https_context = ssl._create_unverified_context
        ret = 0
        for url in urls:
            try:
                response = urllib.request.urlopen(url)
            except:
                response = urllib.request.urlopen(urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"}))
            ret = ret + int(response.getheader('Content-Length', default = 0))
        ssl._create_default_https_context = ssl_back
        return str(ret)
    except:
        raise
        winman_warning('cannot estimate package size -- ' + name)
        return 0

def human_readable_size(size):
    size_orig = size
    suffices = ['B', 'KiB', 'MiB', 'GiB', 'TiB']
    cur = 0
    while True:
        if size < 1024:
            return ("%.2f" % size) + ' ' + suffices[cur]
        cur += 1
        if cur >= len(suffices):
            return str(size_orig) + ' ' + suffices[0]
        size /= 1024

def download_file(name, version, url, filename, progress_function):
    try:
        dir = os.path.dirname(filename)
        if not os.path.exists(dir):
            os.makedirs(dir)
        file = open(filename, 'wb')
        ssl_back = ssl._create_default_https_context
        ssl._create_default_https_context = ssl._create_unverified_context
        try:
            response = urllib.request.urlopen(url)
        except:
            response = urllib.request.urlopen(urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"}))
        ssl._create_default_https_context = ssl_back
        size = int(response.getheader('Content-Length', default = 0))
        size_dl = 0
        block_size = 8192
        start = time.time()
        _done = False
        while True:
            buffer = response.read(block_size)
            if buffer:
                size_dl += len(buffer)
                file.write(buffer)
                _progress = size_dl / size if size else 0
            else:
                _progress = 1
                _done = True
            now = time.time()
            duration = now - start
            speed = size_dl / duration if duration else 1
            progress_function(name + '-' + version,
                auto_prefix(human_readable_size(size_dl), 11) + ' ' + auto_prefix(human_readable_size(speed), 11) + '/s ' + time.strftime('%M:%S', time.gmtime(now - start)), _progress, _done)
            if not buffer:
                break
    except:
        raise
        raise WinmanError('failed retrieving file: ' + url)

def get_package_regex_subs(name, updater):
    html_content = ''
    url = updater['packages'][name]['version']['url']
    ret = {}
    try:
        try:
            html_content = urllib.request.urlopen(url).read().decode(encoding = 'UTF-8', errors = 'ignore')
        except:
            html_content = urllib.request.urlopen(urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})).read().decode(encoding = 'UTF-8', errors = 'ignore')
        regex = re.compile(updater['packages'][name]['version']['regex'], re.M | re.S)
        ret['version'] = regex.findall(html_content)[int(updater['packages'][name]['version']['match'])]
        # join result if necessary
        if isinstance(ret['version'], tuple):
            ret['version'] = ''.join(ret['version'])
    except:
        raise WinmanError('cannot retrieve package version -- ' + name)
    cur_sub = ''
    try:
        if 'subs' in updater['packages'][name]:
            # urls in subs can already use $version
            regex_url = re.compile(re.escape('$version'))
            for sub_k, sub_v in updater['packages'][name]['subs'].items():
                sub_url = regex_url.sub(lambda x: ret[x.group()[1:]], updater['packages'][name]['subs'][sub_k]['url'])
                if sub_url != url:
                    url = sub_url
                    html_content = urllib.request.urlopen(url).read().decode(encoding = 'UTF-8')
                regex = re.compile(sub_v['regex'], re.M | re.S)
                ret[sub_k] = regex.findall(html_content)[int(updater['packages'][name]['subs'][sub_k]['match'])]
                # join result if necessary
                if isinstance(ret[sub_k], tuple):
                    ret[sub_k] = ''.join(ret[sub_k])
    except:
        raise WinmanError('cannot retrieve package sub \'' + cur_sub + '\' -- ' + name)
    return ret

def get_package_regex_checksums(name, updater, package_subs):
    ret = {}
    if 'checksums' in updater['packages'][name]:
        if 'x86' in updater['packages'][name]['checksums']:
            ret['x86_checksums'] = {}
            for k, v in updater['packages'][name]['checksums']['x86'].items():
                url = expand_package_string_subs(v['url'], package_subs)
                try:
                    html_content = urllib.request.urlopen(url).read().decode(encoding = 'UTF-8')
                except:
                    html_content = urllib.request.urlopen(urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})).read().decode(encoding = 'UTF-8')
                regex = re.compile(v['regex'], re.M | re.S)
                ret['x86_checksums'][k] = regex.findall(html_content)[int(updater['packages'][name]['checksums']['x86'][k]['match'])]
        if 'x86_64' in updater['packages'][name]['checksums']:
            ret['x86_64_checksums'] = {}
            for k, v in updater['packages'][name]['checksums']['x86_64'].items():
                url = expand_package_string_subs(v['url'], package_subs)
                try:
                    html_content = urllib.request.urlopen(url).read().decode(encoding = 'UTF-8')
                except:
                    html_content = urllib.request.urlopen(urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})).read().decode(encoding = 'UTF-8')
                regex = re.compile(v['regex'], re.M | re.S)
                ret['x86_64_checksums'][k] = regex.findall(html_content)[int(updater['packages'][name]['checksums']['x86_64'][k]['match'])]
    return ret

def get_package_deps(name, repo):
    """ return tuple of form (innerdeps, outerdeps)"""
    ret = [set(), set()]
    if 'outerdeps' in repo['packages'][name]:
        ret[1] = set(repo['packages'][name]['outerdeps'])
    if 'innerdeps' in repo['packages'][name]:
        for idep in repo['packages'][name]['innerdeps']:
            ret[0].add(idep)
            idep_i, idep_o = get_package_deps(idep, repo)
            ret[0] |= idep_i
            ret[1] |= idep_o
    return ret

def ask_yes_no(string, default = 'yes'):
    while True:
        var = str(input(string + '? [' + ('Y/n' if default == 'yes' else 'y/N' if default == 'no' else 'y/n') + '] '))
        if not var and default == 'yes':
            return True
        elif not var and default == 'no':
            return False
        elif var == 'Y' or var == 'y':
            return True
        elif var == 'N' or var == 'n':
            return False

def get_system_bits():
    return 64 if sys.maxsize > 2**32 else 32

def get_package_file_entry(name, repo):
    return 'x86_64' if get_system_bits() == 64 and 'x86_64' in repo['packages'][name] else 'x86'

def get_terminal_size_windows(fallback = None):
    res = None
    try:
        from ctypes import windll, create_string_buffer
        # stdin handle is -10
        # stdout handle is -11
        # stderr handle is -12
        h = windll.kernel32.GetStdHandle(-12)
        csbi = create_string_buffer(22)
        res = windll.kernel32.GetConsoleScreenBufferInfo(h, csbi)
    except:
        return None
    if res:
        import struct
        (bufx, bufy, curx, cury, wattr, left, top, right, bottom, maxx, maxy) = struct.unpack("hhhhHhhhhhh", csbi.raw)
        sizex = right - left + 1
        sizey = bottom - top + 1
        return sizex, sizey
    else:
        return fallback

def expand_package_info(names, repo):
    ret = {}
    for name in names:
        ret[name] = expand_package_dict(name, repo)
    for k, v in ret.items():
        pattern = re.compile('|'.join(re.escape(key) for key in v.keys()))
        v['package_file_url'] = pattern.sub(lambda x: v[x.group()], repo['packages'][k][get_package_file_entry(k, repo)])
        v['package_paths'] = [pattern.sub(lambda x: v[x.group()], path) for path in repo['packages'][k]['paths']]

        size_str = get_package_file_entry(k, repo) + '_size'
        if size_str in repo['packages'][k]:
            v['package_file_size'] = repo['packages'][k][size_str]
        extension = ''
        if repo['packages'][k]['kind'] in ['nsis', 'innosetup']:
            extension = '.exe'
        if repo['packages'][k]['kind'] in ['msi']:
            extension = '.msi'
        if repo['packages'][k]['kind'] in ['zip']:
            extension = '.zip'
        v['package_filename'] = 'cache\\' + k + '-' + v['$version'] + extension
        if 'zipdeps' in repo['packages'][k]:
            v['zipdeps'] = list(zip([pattern.sub(lambda x: v[x.group()], zipdep) for zipdep in repo['packages'][k]['zipdeps'][get_package_file_entry(k, repo)]], ['cache\\' + k + '-' + v['$version'] + '_zip' + str(x) for x in range(len(repo['packages'][k]['zipdeps'][get_package_file_entry(k, repo)]))]))
    return ret

def expand_package_string(name, repo, string):
    edict = expand_package_dict(name, repo)
    pattern = re.compile('|'.join(re.escape(key) for key in edict.keys()))
    return pattern.sub(lambda x: edict[x.group()], string)

def expand_package_dict(name, repo):
    ret = {
        '$version': repo['packages'][name]['version'],
        '$instdir': (os.environ['ProgramFiles(x86)'] if get_system_bits() == 64 and 'x86_64' not in repo['packages'][name] else os.environ['ProgramFiles']) + '\\' + name
    }
    if 'subs' in repo['packages'][name]:
        for sub_k, sub_v in repo['packages'][name]['subs'].items():
            ret['$' + sub_k] = sub_v
    return ret

def expand_package_string_subs(string, package_subs):
    pattern = re.compile('|'.join(re.escape('$' + key) for key in package_subs.keys()))
    return pattern.sub(lambda x: package_subs[x.group()[1:]], string)

def add_to_path(new_entries):
    proc = subprocess.Popen('reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v "Path"', stdout = subprocess.PIPE)
    (out, err) = proc.communicate()
    proc.wait()
    is_expand_sz = False
    regex = re.compile('.*Path\s+REG_EXPAND_SZ\s+([^\r\n]*)', re.M | re.S)
    cur_path = regex.findall(out.decode(encoding = 'Utf-8'))
    if cur_path:
        is_expand_sz = True
        cur_path = cur_path[0]
    else:
        regex = re.compile('.*Path\s+REG_SZ\s+([^\r\n]*)', re.M | re.S)
        cur_path = regex.findall(out.decode(encoding = 'Utf-8'))[0]
    regex = re.compile('([^;]*);*')
    # conversion to set -> remove duplicates
    path_entries = list(set([x for x in regex.findall(cur_path) if x]))
    # check for non-existing entries
    path_entries_to_remove = []
    for entry in path_entries:
        if not os.path.isdir(entry):
            if ask_yes_no(':: Remove non existing Path entry \'' + entry + '\''):
                path_entries_to_remove.append(entry)
    path_entries_new = set(filter(lambda x: x not in path_entries_to_remove, path_entries)) | set(new_entries)
    if not set(path_entries) == path_entries_new:
        if ask_yes_no(':: Add new Path entries ' + ';'.join(path_entries_new - set(path_entries))):
            elevated_execute('reg.exe', 'add "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /f /v "Path" /t ' + ('REG_EXPAND_SZ' if is_expand_sz else 'REG_SZ') + ' /d "' + ';'.join(list(path_entries_new)) + '"')
            #print(':: Refreshing environment variables...')
            #proc = subprocess.Popen('set.exe Path="' + ';'.join(list(path_entries_new)) + '" > nul', shell = True)
            #proc.wait()
            #os.environ['Path'] = '"' + ';'.join(list(path_entries_new)) + '"'
            win32gui.SendMessage(win32con.HWND_BROADCAST, win32con.WM_SETTINGCHANGE, 0, 'Environment')

def elevated_execute(command, args = None):
    # SEE_MASK_NOASYNC = 256 and SEE_MASK_NOCLOSEPROCESS = 64
    if args:
        hProcess = win32shell.ShellExecuteEx(fMask = 256 + 64, lpVerb = 'runas', lpFile = command, lpParameters = args)['hProcess']
    else:
        hProcess = win32shell.ShellExecuteEx(fMask = 256 + 64, lpVerb = 'runas', lpFile = command)['hProcess']
    win32event.WaitForSingleObject(hProcess, -1)

def print_help_entry(first_str, max_len_first_str, rest):
    width = get_terminal_size_windows((70,10))[0] - 1
    # one space after first_str
    max_len_first_str += 1
    if width <= max_len_first_str:
        regex = re.compile('(^\s*)')
        indent = 0
        matches = regex.findall(first_str)
        if matches:
            indent = len(matches[0])
        wrapped = textwrap.wrap(first_str[indent:], width = width - indent)
        print(' ' * indent + wrapped[0] + ('\n    ' if len(wrapped) > 1 else '') + '\n    '.join(map(lambda x: ' ' * indent + (x[1:] if x[0].isspace() else x), textwrap.wrap(first_str[len(wrapped[0]) + indent:], width = width - indent - 4))))
        indent += 4 if len(wrapped) == 1 else 8
        if rest:
            print('\n'.join(map(lambda x: ' ' * indent + x, textwrap.wrap(rest, width = max(1, width - indent)))))
    else:
        wrapped = textwrap.wrap(rest, width = width - max_len_first_str)
        print(first_str + ((' ' * (max_len_first_str - len(first_str)) + wrapped[0]) if wrapped else '') + ('\n' if len(wrapped) > 1 else '') + ('\n'.join(map(lambda x: (' ' * max_len_first_str) + x, wrapped[1:])) if len(wrapped) > 1 else ''))
